[![pipeline status](/../badges/master/pipeline.svg)](/../pipelines)

* [Latest PDF online](/../-/jobs/artifacts/master/file/paper.pdf?job=build)
    * [Build Log](/../-/jobs/artifacts/master/file/build_paper.log?job=build)

# Deep modeling for self-reproducing robots
> This is a paper about how to use deep modeling to describe a self-reproducing robots control system

[ShareLaTex link](https://tex.zih.tu-dresden.de/7733911278qxxqdzbkcnzf
) 